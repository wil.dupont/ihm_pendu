import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData ;
import javafx.scene.control.ButtonType ;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button boutonInfo;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("/usr/share/dict/american-english", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        this.panelCentral = new BorderPane(); //*
        this.bJouer = new Button("Lancer une partie");
        ControleurLancerPartie controleur = new ControleurLancerPartie(this.modelePendu,this);        
        this.bJouer.setOnAction(controleur);
        // A terminer d'implementer
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);//*
        // fenetre.setCenter(this.fenetreAccueil());
        return new Scene(fenetre, 800, 800);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        // Ici ajouter BorderPane        
        BorderPane banniere = new BorderPane();
        Text text = new Text("Jeu du Pendu");
        text.setFont(Font.font("Arial", 30));
        banniere.setLeft(text);
        banniere.setPrefSize(800, 100);
        banniere.setStyle("-fx-background-color: #E6E6FA");
        banniere.setPadding(new Insets(30,10,10,30));

        ImageView imv= new ImageView("home.png");
        this.boutonMaison = new Button("",imv);

        ImageView imv1= new ImageView("parametres.png");
        this.boutonParametres = new Button("",imv1);

        ImageView imv2= new ImageView("info.png");
        this.boutonInfo = new Button("",imv2);

        HBox hb= new HBox();
        hb.getChildren().addAll(this.boutonMaison,this.boutonParametres,this.boutonInfo);
        banniere.setRight(hb);

        //diminuer taille image
        imv.setFitHeight(30);
        imv.setFitWidth(30);
        imv1.setFitHeight(30);
        imv1.setFitWidth(30);
        imv2.setFitHeight(30);
        imv2.setFitWidth(30);

        // espacer les boutons
        hb.setSpacing(5);

        // ImageView imv2= new ImageView("home.png");
        // this.boutonMaison = new Button("",imv2);
        


        return banniere;
    }

    private Pane fenetreAccueil(){   
        VBox res = new VBox();
        Button bouton1 = new Button("Lancer une partie");

        // faire TitledPane pour choisir le niveau de difficulté
        TitledPane tp = new TitledPane();
        //faire derouler le menu déroulant

        tp.setText("Niveau de difficulté");
        tp.setExpanded(true);
        tp.setCollapsible(false);

        // créer radiobuttons dans un titledpane
        RadioButton rb1 = new RadioButton("Facile");
        RadioButton rb2 = new RadioButton("Moyen");
        RadioButton rb3 = new RadioButton("Difficile");
        RadioButton rb4 = new RadioButton("Expert");

        // créer un groupe de boutons radio
        ToggleGroup togg = new ToggleGroup();
        rb1.setToggleGroup(togg);
        rb2.setToggleGroup(togg);
        rb3.setToggleGroup(togg);
        rb4.setToggleGroup(togg);

        // créer un VBox pour les boutons radio
        VBox vbo = new VBox();
        vbo.getChildren().addAll(rb1, rb2, rb3,rb4);

        // ajouter le VBox dans le TitledPane
        tp.setContent(vbo);


        res.getChildren().addAll(bJouer,tp);
        res.setPadding(new Insets(20,10,10,10));
        res.setSpacing(10);
        return res;
    }
    // /**
     // * @return le panel du chronomètre
     // */
    private TitledPane leChrono(){
        // A implementer
        TitledPane res = new TitledPane();
        return res;
    }

    // /**
     // * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     // *         de progression et le clavier
     // */
     
    private Pane fenetreJeu(){
        BorderPane res = new BorderPane();
        Text difficulte = new Text("Niveau " + this.leNiveau.getText());
        res.setRight(difficulte);
        VBox centre = new VBox();
      
        centre.getChildren().addAll(this.motCrypte,this.pg,this.dessin,this.clavier);
        centre.setPadding(new Insets(15));
        centre.setSpacing(10);
        centre.setAlignment(Pos.TOP_CENTER);

        VBox droit = new VBox();
        Button Newmots = new Button("Nouveau mot");
        Newmots.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        droit.getChildren().addAll(difficulte, leChrono());
















        return res;
    }

    // /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
    

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        this.panelCentral.setCenter(fenetreAccueil());

    }
    
    public void modeJeu(){
        this.panelCentral.setCenter(fenetreJeu());
    }
    
    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie(){
        this.modeJeu();
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        // A implementer
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        // A implémenter
        return null; // A enlever
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\nEtes-vous sûr de vouloir quitter ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Règle du jeu : \ndeviner le mots avec un nombre de tentative limité,lettre par lettre.", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);        
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("Affronte mon pendu");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}